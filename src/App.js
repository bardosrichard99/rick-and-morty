import { Navigate, Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Profile from "./pages/Profile";
import Navbar from "./components/NavBar";
import { ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="flex flex-col justify-between">
      <ToastContainer />
      <Navbar />
      <main className="container mx-auto px-3 pb-12">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/profile/:charId" element={<Profile />} />
          <Route path="/*" element={<Navigate to='/'/>} />
        </Routes>
      </main>
    </div>
  );
}

export default App;
