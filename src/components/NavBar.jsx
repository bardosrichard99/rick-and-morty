import React from 'react'
import { Link } from "react-router-dom";
import { RiMovieLine } from "react-icons/ri";
import { BiHomeAlt2 } from "react-icons/bi";

function NavBar() {
  return (
    <nav className="mb-5 shadow-lg bg-amber-100 sticky top-0 z-50">
      <div className="flex mx-auto">
        <div className="w-5/6 px-2 m-2">
          <RiMovieLine className="inline pr-2 text-3xl" />
          <Link to="/" className="text-lg font-bold align-middle">
            Rick & Morty
          </Link>
        </div>
        <div className="w-1/6 px-2 mx-2">
            <Link to="/">
              <BiHomeAlt2 className='h-10' size={24}/>
            </Link>
        </div>
      </div>
    </nav>
  )
}

export default NavBar
