import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { IoIosArrowBack } from "react-icons/io";
import { IoIosArrowForward } from "react-icons/io";

function Home() {
  const [data, setData] = useState([]);
  const [nameSearch, setNameSearch] = useState("");
  const [pageNum, setPageNum] = useState(1);

  useEffect(() => {
    axios
      .get(
        `https://rickandmortyapi.com/api/character/?page=${pageNum.toString()}&name=${nameSearch}`
      )
      .then((res) => {
        setData(res.data.results);
      })
      .catch((err) => {
        //console.log(err);
        setData([]);
      });
  }, [nameSearch, pageNum]);

  return (
    <div className="mx-10 my-5">
      <div className="flex justify-end mb-5">
        <label htmlFor="table-search" className="sr-only">
          Search
        </label>
        <div className="relative">
          <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg
              className="w-5 h-5 text-gray-500 dark:text-gray-400"
              aria-hidden="true"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              ></path>
            </svg>
          </div>
          <input
            type="text"
            id="table-search"
            value={nameSearch}
            onChange={(e) => setNameSearch(e.target.value)}
            className="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-500 focus:border-blue-500"
            placeholder="Search for items"
          />
        </div>
      </div>

      <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3">
                Avatar
              </th>
              <th scope="col" className="px-6 py-3">
                Name
              </th>
              <th scope="col" className="px-6 py-3">
                Species
              </th>
              <th scope="col" className="px-6 py-3">
                Status
              </th>
              <th scope="col" className="px-6 py-3"></th>
            </tr>
          </thead>
          {data.length === 0 ? (
            <tbody>
              <tr>
                <td>
                  <p className="m-4">No results</p>
                </td>
              </tr>
            </tbody>
          ) : (
            <tbody>
              {data.map((character) => (
                <tr
                  key={character.id}
                  className={`${
                    character.id % 2 === 1
                      ? "bg-white dark:bg-gray-900"
                      : "bg-gray-50 dark:bg-gray-800"
                  } border-b  dark:border-gray-700`}
                >
                  <th
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    <img
                      src={character.image}
                      className="h-28"
                      alt="character avatar"
                    />
                  </th>
                  <td className="px-6 py-4">{character.name}</td>
                  <td className="px-6 py-4">{character.species}</td>
                  <td className="px-6 py-4">{character.status}</td>
                  <td className="px-6 py-4">
                    <Link
                      to={`/profile/${character.id}`}
                      className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
                    >
                      View details
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          )}
        </table>
      </div>
      <nav
        className="flex items-center justify-between pt-4"
        aria-label="Table navigation"
      >
        <span className="text-sm font-normal text-gray-500 dark:text-gray-400">
          Showing <span className="font-semibold text-gray-900">{(pageNum-1)*20+1}-{(pageNum-1)*20+20}</span> of
          <span className="font-semibold text-gray-900"> 826</span>
        </span>
        <ul className="inline-flex items-center -space-x-px">
          <li>
            <button
              onClick={() => setPageNum(pageNum - 1)}
              disabled={pageNum === 1}
              className={`block px-3 py-2 ml-0 leading-tight text-gray-500 border border-gray-300 rounded-l-lg ${pageNum > 1 ? 'hover:bg-gray-100 hover:text-gray-700 bg-white':'bg-gray-100 cursor-not-allowed'} `}
            >
              <IoIosArrowBack className="h-5" />
            </button>
          </li>
          <li>
          <button
            className={`block px-3 py-2 leading-tight text-gray-500 bg-white border border-gray-300 cursor-text`}
          >
            Page {pageNum}
          </button>
        </li>
          <li>
            <button
              onClick={() => setPageNum(pageNum + 1)}
              className="block px-3 py-2 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg hover:bg-gray-100 hover:text-gray-700 "
            >
              <IoIosArrowForward className="h-5" />
            </button>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Home;
