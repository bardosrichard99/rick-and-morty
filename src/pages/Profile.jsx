import { useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { GiDeathSkull } from "react-icons/gi";
import { GiLifeBar } from "react-icons/gi";
import { toast } from 'react-toastify';

function Profile() {
  const params = useParams();
  const [charData, setCharData] = useState([]);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${params.charId}`)
      .then((res) => {
        setCharData(res.data);
      })
      .catch((err) => {
        toast.error(err);
      });
  }, [params.charId]);

  const episodes = Array(51);
  episodes.fill(false, 0, 51)
  if (charData.episode) {
    for (let i = 0; i < charData.episode.length; i++) {
      const number = charData.episode[i].substring(
        charData.episode[i].lastIndexOf("/")+1
      );
      
      episodes[number-1] = true;
    }
  }

  return (
    <div className="w-full mx-auto lg:w-10/12 ">
      <div className="mb-10">
        <Link
          to="/"
          className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
        >
          Back to table
        </Link>
      </div>

      <div className="flex gap-x-10 justify-around">
        <div className="w-2/6">
          <img
            src={charData.image}
            alt="Character image"
            className="rounded-lg"
          />
        </div>

        <div className="w-4/6 p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
          <h2 className="text-amber-500 text-4xl mb-6 font-medium">
            {charData.name}
          </h2>
          <p
            className={`${
              charData.gender === "Male"
                ? "text-sky-600"
                : charData.gender === "Female"
                ? "text-purple-600"
                : ""
            } font-medium`}
          >
            {charData.gender}
          </p>
          <p className="text-gray-600 font-small my-2">{charData.species}</p>
          {charData.status === "Dead" ? (
            <GiDeathSkull className="h-10 text-black" size={30} />
          ) : (
            <GiLifeBar className="h-10 text-red-700" size={30} />
          )}
        </div>
      </div>

      <h2 className="mt-10 text-2xl mb-6 font-medium">Episodes</h2>
      <div className="flex flex-wrap justify-normal gap-y-4">
      {episodes.map((episode, index) => (
        <span key={index} className={`${episode ? 'bg-green-100 text-green-800' : 'bg-red-100 text-red-800'} text-sm font-medium mr-2 px-2.5 py-0.5 rounded w-8 text-center`}>{index+1}</span>
      ))}
      </div>
    </div>
  );
}

export default Profile;
